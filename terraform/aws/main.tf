provider "aws" {
	region = "eu-west-3"
}

resource "aws_instance" "esme" {
	ami = "ami-06b6c7fea532f597e"
	instance_type = "t2.micro"
	tags = {
		"Name" = "esme"
	}
}
